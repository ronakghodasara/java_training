package com.pack2;

import java.util.HashSet;
import java.util.Set;

public class Demo2 {

	public static void main(String[] args) {
		Emp emp1 = new Emp(100, "ronak", 1000);
		Emp emp2 = new Emp(106, "emp2", 1050);
		Emp emp3 = new Emp(120, "emp3", 5000);
		Emp emp4 = new Emp(256, "emp4", 1099);
		Emp emp5 = new Emp(220, "emp5", 1099);
		Emp emp6 = new Emp(120, "emp3", 5000);

		
		Set<Emp> _set = new HashSet<Emp>();
		
		_set.add(emp1);
		_set.add(emp2);
		_set.add(emp3);
		_set.add(emp4);
		_set.add(emp5);
		_set.add(emp6); // ignored
		_set.add(emp2);
		_set.add(emp5);
		
		for (Emp emp : _set) {
			System.out.println(emp);
		}
	}

}
