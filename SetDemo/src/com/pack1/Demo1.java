package com.pack1;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("unused")
public class Demo1 {

	public static void main(String[] args){
		Set<String> str_set = new HashSet<String>();
		//Set<String> str_set = new LinkedHashSet<String>();
		//Set<String> str_set = new TreeSet<String>();
		
		str_set.add("abc");
		str_set.add("dfg");
		str_set.add("hgt");
		str_set.add("uyg");
		str_set.add("asf");
		str_set.add("abc");
		str_set.add("kjh");
		str_set.add("hgt");
		str_set.add("edc");
		str_set.add("nhy");
		
		for (String _str : str_set) {
			System.out.println(_str);
		}
	}	
}
