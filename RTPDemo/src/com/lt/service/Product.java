package com.lt.service;

public abstract class Product {

	private int pid;
	private String pname;
	private double price;
	
	
	public Product(int pid, String pname, double price) {
		super();
		this.pid = pid;
		this.pname = pname;
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}

	public abstract String isExpensive();
	
	public String display() {
		return pid + " " + pname + " " + price;
	}
}
