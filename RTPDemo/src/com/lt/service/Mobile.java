package com.lt.service;

public class Mobile extends Product{
	
	private String mtype;
	
	public Mobile(int pid, String pname, double price, String mtype) {
		super(pid, pname, price);
		this.mtype = mtype;
	}

	@Override
	public String isExpensive() {
		
		if(getPrice() >= 15000) {
			return "Expensive";
		} else if (getPrice() < 5000) {
			return "Cheap";
		} else {
			return "Average";
		}
	}

	@Override
	public String display() {
		return super.display() + " " + mtype;
	}
}
