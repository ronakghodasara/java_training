package com.lt.service;

public class Computer extends Product{

	private String HDD_type;

	public Computer(int pid, String pname, double price, String hDD_type) {
		super(pid, pname, price);
		HDD_type = hDD_type;
	}

	@Override
	public String isExpensive() {
		if(getPrice() >= 60000) {
			return "Expensive";
		} else if (getPrice() < 20000) {
			return "Cheap";
		} else {
			return "Average";
		}
	}

	@Override
	public String display() {
		return super.display() + " " + HDD_type;
	}
}
