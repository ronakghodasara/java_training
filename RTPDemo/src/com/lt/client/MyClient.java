package com.lt.client;

import com.lt.service.Computer;
import com.lt.service.Mobile;
import com.lt.service.Product;

public class MyClient {

	public static void main(String[] args) {
		
		Product[] prod_arr = new Product[4];
		
		prod_arr[0] = new Mobile(1001, "Samsung Galaxy", 12000, "Touch");
		prod_arr[1] = new Mobile(1002, "Moto X", 30000, "Quarty");
		prod_arr[2] = new Computer(1003, "Lenovo", 35000, "DDR3");
		prod_arr[3] = new Computer(1004, "HP", 75000, "DDR2");
		
		
		System.out.println("\nProduct Report\n");
		for(Product p : prod_arr) {
			System.out.println(p.display());
			System.out.println(p.isExpensive());
			System.out.println("\n*****************************************\n");
		}
	}
}
