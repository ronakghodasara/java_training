package com.lt.service;

public class Car {

	@SuppressWarnings("unused")
	private String carName;
	@SuppressWarnings("unused")
	private double price;
	
	public Car() {
		// super() will be added in 1st line by compiler.
		//at least one constructor should call super().
		System.out.println("No argument constructor call.");
	}
	
	public Car(String carName) {
		//calling this() instead of super().
		this(); // must be in 1st line
		System.out.println("One argument constructor call. : " + carName);
	}
	
	public Car(String carName, double price) {
		this(carName);
		this.price = price;
		
		System.out.println("Two argument constructor call. : " + price);
	}
}
