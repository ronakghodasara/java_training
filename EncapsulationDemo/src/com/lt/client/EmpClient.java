package com.lt.client;

import com.lt.service.Emp;

public class EmpClient {

	public static void main(String[] args) {
		
		Emp emp = new Emp();
		
		try {
			emp.setEmp_name("RONAK");
			emp.setAge(24);
			
			System.out.println(emp.getCmp_name());
			System.out.println(emp.getEmp_name());
			System.out.println(emp.getAge());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

}
