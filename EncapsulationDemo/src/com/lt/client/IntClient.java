package com.lt.client;

import java.util.Scanner;

import com.lt.service.Interest;
import com.lt.util.MyUtil;

public class IntClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the amount:\n");
		double amt = scan.nextDouble();
		
		System.out.println("Enter the Time Period(Years):\n");
		int time = scan.nextInt();
		
		System.out.println("Enter the rate:\n");
		float rate = scan.nextFloat();
		
		try {
			// Creating instance of class Interest
			Interest obj = new Interest();
			
			obj.setAmt(amt);
			obj.setRate(rate);
			obj.setYears(time);
			
			System.out.println("Simple Interest : " + MyUtil.roundTwoDecPlaces(obj.calcSI()));
			System.out.println("Compound Interest : " + MyUtil.roundTwoDecPlaces(obj.calcCI()));
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		scan.close();
	}

}
