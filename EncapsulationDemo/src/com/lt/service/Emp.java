package com.lt.service;

public class Emp {

	private String cmp_name = "L&T TS";
	private String emp_name;
	private byte age;
	
	/**
	 * setCmp_name not defined to make Cmp_nmae read only 
	 * @return
	 */
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) throws Exception {
		if (!emp_name.matches("[a-zA-Z]{3,5}")) {
			throw new Exception("Name must be 3 to 5 char long alphabets.");
		}
		this.emp_name = emp_name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) throws Exception {
		if (age < 21 || age > 45) {
			throw new Exception("Age Should be Between 21 to 45.");
		}
		this.age = (byte)age;
	}
	public String getCmp_name() {
		return cmp_name;
	}
	
	
}
