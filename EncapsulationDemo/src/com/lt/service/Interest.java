package com.lt.service;

public class Interest {
	
	// instance members
	// state (only instance variables can have)
	private double amt;
	private int years;
	private float rate;
	
	
	
	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) throws Exception {
		if (amt < 1000) {
			throw new Exception("Amount must be min 1000.");
		}
		this.amt = amt;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) throws Exception {
		if (years < 1) {
			throw new Exception("Time period should be min 1 year.");
		}
		this.years = years;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) throws Exception {
		if (rate <= 0) {
			throw new Exception("Rate must be greter then 0.");
		}
		this.rate = rate;
	}

	//instance methods
	//specific to application
	/**
	 * @return
	 */
	public double calcSI() {
		return amt * years * rate / 100;
	}
	
	public double calcCI() {
		return amt * Math.pow((1 + rate/100), years) - amt;
	}

}
