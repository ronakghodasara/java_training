import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class MyRmiserver {

	public static void main(String[] args) throws RemoteException, MalformedURLException {
		ISer ser = new SerImpl();
		
		Naming.rebind("rmi://localhost:5000/lt", ser);

		System.out.println("Server Ready.....\n");
	}
}
