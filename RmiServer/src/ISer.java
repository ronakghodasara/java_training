import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ISer extends Remote{

	public String sayHello(String str) throws RemoteException;
	public double calcSi(double p, int n, double r) throws RemoteException;
}
