import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

//UnicastRemoteObject.. for support for distributed env.
public class SerImpl extends UnicastRemoteObject implements ISer{

	protected SerImpl() throws RemoteException {
		super();
	}

	@Override
	public String sayHello(String str) throws RemoteException {
		return "ronak welcomes " + str;
	}

	@Override
	public double calcSi(double p, int n, double r) throws RemoteException {
		return p * r * n / 100;
	}

}
