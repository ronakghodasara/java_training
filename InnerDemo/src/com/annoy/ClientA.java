package com.annoy;

public class ClientA {

	private static String str = "Check";
	
	static Flyable eagle = new Flyable() {
				
		@Override
		public void fly() {
			System.out.println(str + " eagle fly");
		}
	};
	
	static Flyable rnk = new Flyable() {
		
		@Override
		public void fly() {
			System.out.println(str + " rnk can not fly");
			
		}
	};
	
	static Flyable car = new Flyable() {
		
		@Override
		public void fly() {
			System.out.println(str + " car can fly");
			
		}
	};
	
	public static void main(String[] args) {

		eagle.fly();
		rnk.fly();
		car.fly();
		
	}

}
