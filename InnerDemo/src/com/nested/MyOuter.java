package com.nested;

public class MyOuter {

	private double price = 7000000;
	private static String cname = "BMW";
	
	public static class MyInner{
	
		public void display(){
			//instance member can't access
			//System.out.println("Car Price : " + price);
			System.out.println("Car Name : " + cname);
		}
	}
}
