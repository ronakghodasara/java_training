import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class MyClient {

	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		
		ISer ser = (ISer) Naming.lookup("rmi://192.168.5.13:5000/lt");
		
		System.out.println(ser.sayHello("rnk"));
		System.out.println(ser.calcSi(1000, 10, 10));
	}

}
