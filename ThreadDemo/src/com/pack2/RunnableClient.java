package com.pack2;

public class RunnableClient {

	public static void main(String[] args) throws InterruptedException {
		
		
		MyRunnable r1 = new MyRunnable();

		Thread th1 = new Thread(r1);
		th1.setName("Thread1");
		
		Thread th2 = new Thread(r1);
		th2.setName("Thread2");
		
		th1.start();
		th2.start();
		
		th1.join();
		//th2.join();
		
		for(int i=1; i<=2500; i++){
			System.out.println(Thread.currentThread().getName() + " : " + (i * 5));
		}
	}
}
