package com.pack4;

public class ThreadA extends Thread{

	private Account acc;
	
	public ThreadA(Account acc) {
		this.acc = acc;
	}
	
	public void run(){
		acc.doTxns();
	}

}
