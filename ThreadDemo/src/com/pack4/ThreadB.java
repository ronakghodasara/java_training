package com.pack4;

public class ThreadB extends Thread{

	private Account acc;
	
	public ThreadB(Account acc) {
		this.acc = acc;
	}
	
	public void run(){
		acc.doTxns();
	}

}
