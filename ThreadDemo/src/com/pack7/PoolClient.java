package com.pack7;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PoolClient {

	public static void main(String[] args) {
		ExecutorService pool = Executors.newFixedThreadPool(2);
		
		LtRunnable r1 = new LtRunnable("P1");
		LtRunnable r2 = new LtRunnable("P2");
		LtRunnable r3 = new LtRunnable("P3");
		LtRunnable r4 = new LtRunnable("P4");
		LtRunnable r5 = new LtRunnable("P5");
		LtRunnable r6 = new LtRunnable("P6");
		
		pool.execute(r1);
		pool.execute(r2);
		pool.execute(r3);
		pool.execute(r4);
		pool.execute(r5);
		pool.execute(r6);
		
		pool.shutdown();
	}
}
