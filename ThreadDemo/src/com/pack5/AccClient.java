package com.pack5;

public class AccClient {

	public static void main(String[] args) {
		Account acc = new Account();
		ThreadA tha = new ThreadA(acc);
		ThreadB thb = new ThreadB(acc);
		
		tha.setPriority(1);
		thb.setPriority(10);
		
		tha.setName("ThreadA");
		thb.setName("ThreadB");
		
		tha.start();
		thb.start();
	}

}
