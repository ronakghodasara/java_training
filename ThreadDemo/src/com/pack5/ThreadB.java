package com.pack5;

public class ThreadB extends Thread{

	private Account acc;
	
	public ThreadB(Account acc) {
		this.acc = acc;
	}
	
	public void run(){
		synchronized(acc){
			acc.doTxns();
		}
	}

}
