package com.pack5;

public class ThreadA extends Thread{

	private Account acc;
	
	public ThreadA(Account acc) {
		this.acc = acc;
	}
	
	public void run(){
		synchronized(acc){
			acc.doTxns();
		}
	}
}
