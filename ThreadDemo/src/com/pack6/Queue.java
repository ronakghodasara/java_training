package com.pack6;

public class Queue {

	private String data;
	private boolean flag;
	
	public synchronized void put(String str) {
		try {
			if(flag) {
				wait();
			} else {
				data = str;
				flag = true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		notify();
	}
	
	public synchronized void get() {
		try {
			if(!flag) {
				wait();
			} else {
				flag = false;
				System.out.println("Consumed : " + data);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		notify();
	}
}
