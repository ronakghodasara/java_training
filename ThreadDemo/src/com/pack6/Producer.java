package com.pack6;

import java.util.Scanner;

public class Producer extends Thread{

	private Queue que;
	
	public Producer(Queue q) {
		this.que = q;
	}
	
	public void run() {
		Scanner scan = new Scanner(System.in);
		for(int i=0; i<10; i++) {
			System.out.println("Enter Data : \n");
			String str = scan.next();
			que.put(str);
		}
		scan.close();
	}
}
