package com.pack6;

public class QueClient {

	public static void main(String[] args) {
		Queue que = new Queue();
		Producer tha = new Producer(que);
		Consumer thb = new Consumer(que);
		
		tha.setName("Producer");
		thb.setName("Consumer");
		
		tha.start();
		thb.start();
	}

}
