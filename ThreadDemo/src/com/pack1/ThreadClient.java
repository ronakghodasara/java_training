package com.pack1;

public class ThreadClient {

	public static void main(String[] args) {
		
		MyThread th1 = new MyThread();
		th1.setPriority(1);
		th1.setName("Thread1");

		MyThread th2 = new MyThread();
		th2.setPriority(5);
		th2.setName("Thread2");
		
		MyThread th3 = new MyThread();
		th3.setPriority(10);
		th3.setName("Thread3");
		
		th1.start();
		th2.start();
		th3.start();
		
		for(int i=1; i<=1500; i++){
			System.out.println(Thread.currentThread().getName() + " : " + (i * 5));
		}
	}
}
