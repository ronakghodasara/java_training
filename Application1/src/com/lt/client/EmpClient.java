package com.lt.client;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import com.lt.service.EmpExeception;
import com.lt.service.EmpUtil;
import com.lt.service.IEmpSer;
import com.lt.vo.Emp;

public class EmpClient {

	private static Scanner scan = new Scanner(System.in);
	private static IEmpSer ser = EmpUtil.getService();

	public static void main(String[] args) {

		String opt = null;

		do {
			processMenu();
			System.out.println("press y to continue");
			opt = scan.next();
		} while(opt.equals("y"));
	}

	public static void processMenu() {
		System.out.println("1-add\n"
				+ "2-viewall\n"
				+ "3-view by id\n"
				+ "4-remove\n");

		int menu = scan.nextInt();

		switch(menu) {
		case 1:
			addEmployee();
			break;
		case 2:
			viewAll();
			break;
		case 3:
			view();
			break;
		case 4:
			remove();
			break;
		default:
			System.out.println("invalid menu option");
		}

	}
	
	public static void addEmployee() {
		System.out.println("Enter Employee Id : ");
		int eid = scan.nextInt();
		System.out.println("Enter Employee Name : ");
		String name = scan.next();
		System.out.println("Enter Employee salary : ");
		double sal = scan.nextDouble();
		
		Emp emp = new Emp(eid, name, sal);
		
		try {
			ser.add(emp);
			System.out.println("Emp Added :" + emp.getEid());
		} catch (EmpExeception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void viewAll() {
		List<Emp> lst = ser.viewEmployee();
		Collections.sort(lst);
		for (Emp emp : lst) {
			System.out.println(emp);
		}
		System.out.println("______________________________________\n");
	}
	
	public static void view() {
		System.out.println("Enter Employee Id : ");
		int eid = scan.nextInt();
		
		Emp emp = ser.viewEmployee(eid);
		if(emp != null) {
			System.out.println(emp);
		} else {
			System.out.println("Emloyee not found\n");
		}
	}
	
	public static void remove() {
		System.out.println("Enter Employee Id : ");
		int eid = scan.nextInt();
		
		try {
			ser.removeEmp(eid);
			System.out.println("Employee Id " + eid + " removed\n");
		} catch (EmpExeception e) {
			System.out.println(e.getMessage());
		}
	}

}
