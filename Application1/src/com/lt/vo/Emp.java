package com.lt.vo;

public class Emp implements Comparable<Emp> {

	private int eid;
	private String ename;
	private double sal;
	
	public Emp(int eid, String ename, double sal) {
		super();
		this.eid = eid;
		this.ename = ename;
		this.sal = sal;
	}
	
	public int getEid() {
		return eid;
	}

	public String getEname() {
		return ename;
	}

	public double getSal() {
		return sal;
	}

	
	/*
	 * Actual toString method prints object reference.(non-Javadoc)
	 * Override method will print object states.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return eid + " " + ename + " " + sal;
	}

	/*
	 * Hashcode generate hashcode using some algorithm.
	 * but this override methods will take employee id as hashcode.
	 * so new element will be added according to employee id in map
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return eid;
	}

	/*
	 * This is used in add method to add new Employee in map.
	 * Actual equals method will compare object but this overridden method
	 * will compare only employee id and return.
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		Emp emp = (Emp)obj;
		if(this.eid == emp.eid)
			return true;
		else
			return false;
	}

	@Override
	public int compareTo(Emp emp) {
		if(this.eid > emp.eid) {
			return 1;
		} else if (this.eid < emp.eid) {
			return -1;
		} else {
			return 0;
		}
	}

}
