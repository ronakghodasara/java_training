package com.lt.service;

import java.util.List;

import com.lt.vo.Emp;

public interface IEmpSer {

	int add(Emp emp) throws EmpExeception;
	List<Emp> viewEmployee();
	Emp viewEmployee(int eid);
	int removeEmp(int eid) throws EmpExeception;
}
