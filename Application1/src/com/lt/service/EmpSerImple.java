package com.lt.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lt.vo.Emp;

public class EmpSerImple implements IEmpSer{

	private static Map<Integer, Emp> emap;
	
	static {
		emap = new HashMap<Integer, Emp>();
		emap.put(1007, new Emp(1007, "ram", 9000));
		emap.put(1002, new Emp(1002, "tom", 7000));
		emap.put(1005, new Emp(1005, "rnk", 5000));
	}

	@Override
	public int add(Emp emp) throws EmpExeception {
		if(emap.containsKey(emp.getEid())) {
			throw new EmpExeception("Emp Id already exist.");
		}
		emap.put(emp.getEid(), emp);
		return 1;
	}

	@Override
	public List<Emp> viewEmployee() {
		Collection<Emp> col = emap.values();
		List<Emp> lst = new ArrayList<Emp>();
		lst.addAll(col);
		
		return lst;
	}

	@Override
	public Emp viewEmployee(int eid) {
		Emp emp = emap.get(eid);
		return emp;
	}

	@Override
	public int removeEmp(int eid) throws EmpExeception {
		if(false == emap.containsKey(eid)) {
			throw new EmpExeception("Emp id not exist\n");
		}
		emap.remove(eid);
		return 1;
	}
}
