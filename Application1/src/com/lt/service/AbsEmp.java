package com.lt.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbsEmp {

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1); // why??
		}
	}
	
	public Connection getConn() throws SQLException {
		Connection conn = null;
		
		String url = "jdbc:mysql://localhost:3306/test";
		conn = DriverManager.getConnection(url, "root", "root");
		return conn;
	}
	
	public void closeConn(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}
	}
	
	
	
}
