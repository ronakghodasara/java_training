package com.lt.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.lt.vo.Emp;

public class EmpJDBCImple extends AbsEmp implements IEmpSer{



	@Override
	public int add(Emp emp) throws EmpExeception {
		
		Connection conn =null;

		try {
			conn = getConn();

			String sql = "insert into employee values(?,?,?,?)";
			PreparedStatement st = conn.prepareStatement(sql);
			st.setInt(1, emp.getEid());
			st.setInt(1, emp.getEid());
			st.setString(2, emp.getEname());
			st.setDouble(3, emp.getSal());
			st.setString(4, "dr");
			
			int rows = st.executeUpdate();
			
			return rows;
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			closeConn(conn);
		}
		return 0;
	}

	@Override
	public List<Emp> viewEmployee() {
		List<Emp> lst = new ArrayList<Emp>();
		Emp emp = null;
		Connection conn = null;

		try {
			conn = getConn();

			String sql = "select * from employee";
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				emp = new Emp(rs.getInt("eid"),
						rs.getString("ename"),
						rs.getDouble("sal"));

				lst.add(emp);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			closeConn(conn);
		}

		return lst;
	}

	@Override
	public Emp viewEmployee(int eid) {
		Emp emp = null;
		Connection conn =null;

		try {
			conn = getConn();

			String sql = "select * from employee where eid= ?";
			PreparedStatement st = conn.prepareStatement(sql);
			st.setInt(1, eid);

			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				emp = new Emp(rs.getInt("eid"),
						rs.getString("ename"),
						rs.getDouble("sal"));

			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			closeConn(conn);
		}

		return emp;
	}

	@Override
	public int removeEmp(int eid) throws EmpExeception {
		// TODO Auto-generated method stub
		return 0;
	}


}
