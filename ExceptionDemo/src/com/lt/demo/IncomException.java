package com.lt.demo;

public class IncomException extends Exception{

	public IncomException(String message) {
		super(message);
	}
}
