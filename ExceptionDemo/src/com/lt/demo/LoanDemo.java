package com.lt.demo;

public class LoanDemo {

	public static void main(String[] args) {
		
		int age = 30;
		double income = 80000;
		String ret = processLoan(age,income);
		System.out.println(ret);
	}

	public static String processLoan(int age, double income) {
		
		try {
			String ret = approveLoan(age, income);
			return ret;
		} catch (AgeException | IncomException e) {
			System.out.println(e.getMessage());
			return "Loan Not approved";
		} finally {
			System.out.println("Finally block");
		}
	}
	
	public static String approveLoan(int age, double income) throws AgeException, IncomException {
		if(age < 20 || age > 45) {
			throw new AgeException("Age must be between 20 and 45.");
		}
		if(income < 600000) {
			throw new IncomException("Income must be gerete then 600000.");
		}
		
		return "loan approved";
	}
}
