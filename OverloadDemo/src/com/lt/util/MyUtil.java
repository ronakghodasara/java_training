package com.lt.util;

public class MyUtil {
	
	//Utility method
	/*
	 * roundTwoDecPlaces method is defined as static so can be called in any method
	 * without instance.
	 */
	public static double roundTwoDecPlaces(double val) {
		return Math.round(val * 100)/100;
	}
	
	public static double roundTwoDecPlaces(float val) {
		return Math.round(val * 100)/100;
	}

	public static double roundTwoDecPlaces(int val) {
		return val;
	}
}
