package com.lt.service;

public class Mtable {

	private double num;

	public Mtable(int num) throws Exception {
		super();
		if(num <= 0) {
			throw new Exception("Number must be greter then Zero.");
		}
		this.num = num;
	}
	
	public Mtable(double num) throws Exception {
		super();
		if(num <= 0.0) {
			throw new Exception("Number must be greter then Zero.");
		}
		this.num = num;
	}
	
	public void display() {
		for(int i = 0; i<=10; i++) {
			System.out.printf("%d * %d = %d\n", num, i ,num*i);
		}
	}
	
	public void display(int rows) {
		for(int i = 0; i<=rows; i++) {
			System.out.printf("%d * %d = %d\n", num, i ,num*i);
		}
	}
	
	public void display(int startrow, int endrow) {
		for(int i = startrow; i<=endrow; i++) {
			System.out.printf("%f * %d = %f\n", num, i ,num*(float)i);
		}
	}
}
