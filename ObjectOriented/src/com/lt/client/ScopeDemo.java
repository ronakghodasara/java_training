package com.lt.client;

public class ScopeDemo {

	//instance variable
	int i;
	
	//static variable
	static int j;
	
	//global variable
	// ERROR without initialized
	// Can be initialized in static block
	public static final int MAX;
	// Can be initialized directly
	public static final int MIN = 0;
	
	//Executed once before any other method.
	//Executed First
	static {
		
		System.out.println("Static Block");
		MAX = 10;
	}
	
	//Executed Second
	public static void main(String[] args) {
		
		//must be initialized
		int localVar = 0;
		
		
		System.out.println("MAIN");
		
		ScopeDemo sd = new ScopeDemo();
		
		//not allowed
		//System.out.println(i);
		
		//Access of variables
		System.out.println(sd.i);
		System.out.println(j);
		System.out.println(localVar);
			
	}
	


}
