package com.lt.client;

import com.lt.service.Sales;

public class SalesClient {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		System.out.println(args[0]);
		
		//Sales is immutable class
		Sales s1 = new Sales("Person1",1000);
		System.out.println("Total Sales : " + Sales.totalSales);
		
		Sales s2 = new Sales("Person2",10000);
		System.out.println("Total Sales : " + Sales.totalSales);
		
		Sales s3 = new Sales("Person3",100000);
		System.out.println("Total Sales : " + Sales.totalSales);
		
	}
}
