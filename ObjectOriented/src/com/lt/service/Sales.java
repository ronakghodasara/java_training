package com.lt.service;


//Immutable class (Cause private instance variables)
public class Sales {

	//Instance variables
	
	//This variable define state
	private String name;
	private double amt;
	
	//This variable define Behavior
	
	//static will be shared will all instances,
	//so there will be only one copy of totalSales.
	public static double totalSales;
	
	/*
	 * Parameterized Construct defined to initialize private variables
	 */
	public Sales(String name, double amt) {
		this.name = name;
		this.amt = amt;
		
		System.out.println("Initial name : " + this.name);
		System.out.println("Initial amount : " + this.amt);
		
		totalSales += this.amt;
	}
}
