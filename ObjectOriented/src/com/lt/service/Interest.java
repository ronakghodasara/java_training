package com.lt.service;

public class Interest {
	
	// instance members
	// state (only instance variables can have)
	public double amt;
	public int years;
	public float rate;
	
	//instance methods
	//specific to application
	/**
	 * @return
	 */
	public double calcSI() {
		return amt * years * rate / 100;
	}
	
	public double calcCI() {
		return amt * Math.pow((1 + rate/100), years) - amt;
	}

}
