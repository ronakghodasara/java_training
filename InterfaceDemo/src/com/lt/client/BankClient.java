package com.lt.client;

import com.lt.service.Agent;
import com.lt.service.Atm;
import com.lt.service.BankUtil;

public class BankClient {

	public static void main(String[] args) {
		
		Atm atm = BankUtil.getAtm();
		atm.withdraw();
		atm.getbalance();
		
		Agent agnt = BankUtil.getAgent();
		agnt.clearPDC();
		
	}

}
