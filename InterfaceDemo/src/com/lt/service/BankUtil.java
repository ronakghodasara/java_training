package com.lt.service;

public class BankUtil {

	private static Bank bank = new Bank();
	
	public static Atm getAtm() {
		Atm atm = bank;
		return atm;
	}
	
	public static Agent getAgent() {
		Agent agent = bank;
		return agent;
	}
}
