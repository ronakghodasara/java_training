package com.lt.training;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AddEmp {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.jdbc.Driver");
		
		String url = "jdbc:mysql://localhost:3306/test";
		
		Connection conn = DriverManager.getConnection(url, "root", "root");
		
		String sql = "insert into employee values(?,?,?,?)";
		PreparedStatement st = conn.prepareStatement(sql);
		st.setInt(1, 1007);
		st.setString(2, "edg");
		st.setDouble(3, 7000);
		st.setString(4, "dr");
		
		int rows = st.executeUpdate();
		
		System.out.println("No of rows affected : " + rows);
	}
}
