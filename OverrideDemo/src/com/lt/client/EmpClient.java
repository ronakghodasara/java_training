/**
 * 
 */
package com.lt.client;

import com.lt.vo.Emp;

/**
 * @author Pcadmin
 *
 */
public class EmpClient {
	

	public static void main(String[] args) {
		Emp e1 = new Emp(1001, "RNK", 9000);
		
		System.out.println(e1);
		
		Emp e2 = new Emp(1001, "RNK", 9000);
		
		System.out.println(e2);
		
		//check based on reference so its false
		System.out.println(e1==e2);
		//checking with overriddedn equals metjhs
		System.out.println(e1.equals(e2));
	}

}
