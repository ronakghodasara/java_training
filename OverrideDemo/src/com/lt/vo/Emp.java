package com.lt.vo;

/**
 * @author Pcadmin
 *
 */
public class Emp {

	private int eid;
	private String ename;
	private double sal;
	
	
	/**
	 * @param eid
	 * @param ename
	 * @param sal
	 */
	public Emp(int eid, String ename, double sal) {
		super();
		this.eid = eid;
		this.ename = ename;
		this.sal = sal;
	}


	@Override
	public String toString() {
		return eid + " " + ename + " " + sal;
	}

	@Override
	public boolean equals(Object obj) {
		Emp emp = (Emp) obj;
		return (this.eid == emp.eid) && (this.ename.equals(emp.ename)) && (this.sal == emp.sal);
	}	
}
