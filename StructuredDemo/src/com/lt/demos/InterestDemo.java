package com.lt.demos;

import java.util.Scanner;

public class InterestDemo {
	
	/*
	 * Example of Structured programming demo.
	 * No object oriented concepts not used.
	 * calcSimple method is static so can be called without instance.
	 */

	public static void main(String[] args) {
		// To take data from user
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the amount:\n");
		double amt = scan.nextDouble();
		
		System.out.println("Enter the Time Period(Years):\n");
		int time = scan.nextInt();
		
		System.out.println("Enter the rate:\n");
		float rate = scan.nextFloat();
		
		double si = calcSimple(amt, time, rate);
		
		System.out.println("Simple interest = " + si);
		
		scan.close();
	}
	
	public static double calcSimple(double p, int n, float r)
	{
		return p * r * n / 100;
	}

}
