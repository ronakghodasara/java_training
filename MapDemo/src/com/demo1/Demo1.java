package com.demo1;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Demo1 {

	public static void main(String[] args) {
		
		Map<Integer, String> map = new HashMap<>();
		
		map.put(1001, "usr1");
		map.put(1002, "usr2");
		map.put(1003, "usr3");
		
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Enter the eid :");
		int eid = scan.nextInt();
		
		String str = map.get(eid);
		if(str != null) {
			System.out.println(str);
		} else {
			System.out.println("Emp not found");
		}
		
		scan.close();

	}

}
