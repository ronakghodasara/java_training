package com.demo1;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Demo2 {

	public static void main(String[] args) {
		
		Map<Integer, String> map = new HashMap<>();
		
		map.put(1001, "usr1");
		map.put(1002, "usr2");
		map.put(1003, "usr3");
		
		System.out.println("\nUsing keyset\n");
		
		Set<Integer> set = map.keySet();
		
		for (Integer _key : set) {
			System.out.println("Key : "+ _key + " Value : " + map.get(_key));
		}
		
		System.out.println("\nUsing Values\n");
		
		Collection<String> col = map.values();
		
		for (String _str : col) {
			System.out.println("Value : " + _str);
		}
		
		System.out.println("\nUsing Entry Set\n");
		
		Set<Entry<Integer, String>> _eset = map.entrySet();
		
		for (Entry e : _eset) {
			System.out.println(e.getKey() + " " + e.getValue());
		}
		
		//Iterator<E>
	}
}
