package com.pack2;

import java.util.Comparator;

public class SalComparator implements Comparator<Emp>{

	@Override
	public int compare(Emp emp1, Emp emp2) {
		if(emp1.getSal() > emp2.getSal()) {
			return 1;
		}else if (emp1.getSal() < emp2.getSal()) {
			return -1;
		}else {
			return 0;
		}
	}

}
