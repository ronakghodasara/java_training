package com.pack2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmpClient{

	public static void main(String[] args) {
		Emp emp1 = new Emp(100, "ronak", 1000);
		Emp emp2 = new Emp(106, "emp2", 1050);
		Emp emp3 = new Emp(120, "rgjk", 5000);
		Emp emp4 = new Emp(220, "podf", 1099);
		
		List<Emp> lst = new ArrayList<>();
		
		lst.add(emp1);
		lst.add(emp2);
		lst.add(emp3);
		lst.add(emp4);
		lst.add(emp1);
		lst.add(emp4);
		
		System.out.println("Sort based on Eid");
		Collections.sort(lst);
		
		for(Emp emp:lst) {
			System.out.println(emp);
		}
		
		System.out.println("Sort based on E Name");
		Collections.sort(lst, new NameComparator());
		
		for(Emp emp:lst) {
			System.out.println(emp);
		}
		
		System.out.println("Sort based on Salary");
		Collections.sort(lst, new SalComparator());
		
		for(Emp emp:lst) {
			System.out.println(emp);
		}
	}
}
