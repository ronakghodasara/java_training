package com.pack1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Demo1 {

	public static void main(String[] args) {
		
		List<String> lst =  new ArrayList<>();
		
		lst.add("rnk");
		lst.add("abc");
		lst.add("ronak");
		lst.add("rg");
		
		for(String str : lst) {
			System.out.println(str);
		}
		System.out.println("----------------------------");
		Collections.sort(lst, Collections.reverseOrder());
		
		for(String str : lst) {
			
			System.out.println(str);
		}
		
		System.out.println("----------------------------");
		Iterator<String> it = lst.iterator();
		
		while(it.hasNext()) {
			if (it.next().equals("abc"))
				it.next();
			System.out.println(it.next());//fetches element.
		}
	}
}
