package com.pack1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

@SuppressWarnings("unused")
public class Demo2 {

	public static void main(String[] args) {

		//List<String> lst =  new LinkedList<>();
		//List<String> lst =  new Vector<>();
		List<String> lst =  new ArrayList<>();
		
		lst.add("rg");
		lst.add("abc");
		lst.add("ronak");
		lst.add("rg");
		
		System.out.println(lst);
		lst.add(2, "difg");
		System.out.println(lst);
		lst.remove(3);
		System.out.println(lst);
		
		System.out.println(lst.size());
		
		System.out.println(lst.get(2));
		
		List<String> lst_new =  new ArrayList<>();
		lst_new.add("qwe");
		lst_new.add("uyt");
		
		lst.addAll(lst_new);
		System.out.println(lst);
		
		lst.addAll(2, lst_new);
		System.out.println(lst);
	}

}
