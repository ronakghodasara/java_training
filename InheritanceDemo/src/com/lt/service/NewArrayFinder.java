package com.lt.service;

import java.util.Arrays;

public class NewArrayFinder extends ArrayFinder {

	@Override
	public int search(String str) {
		System.out.println("binary search.");
		return Arrays.binarySearch(arr, str);
	}

	public void checkPelindrome() {
		
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<arr.length; i++) {
			sb = new StringBuffer(arr[i]);
			sb.reverse();
			if(arr[i].equals(sb.toString())) {
				System.out.println(arr[i] + " is palindrome.");
			} else {
				System.out.println(arr[i] + " is not palindrome.");
			}
		}
	}
}
