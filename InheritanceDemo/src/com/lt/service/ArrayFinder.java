package com.lt.service;

import java.util.Arrays;

public class ArrayFinder {

	public static String[] arr = {"Ram" , "Tom", "rnknr"};
	
	static {
		Arrays.sort(arr);
	}
	
	public int search(String str) {
		for(int i=0; i<arr.length; i++) {
			if(arr[i].equals(str)) {
				return i;
			}
		}
		return -1;
	}
	
	public void display() {
		for(int i=0; i<arr.length; i++) {
			System.out.println(i + "-->" + arr[i]);
		}
	}
}
