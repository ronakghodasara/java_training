package com.lt.client;

import com.lt.service.NewArrayFinder;

public class ClientFinder {

	public static void main(String[] args) {
		NewArrayFinder obj = new NewArrayFinder();
		
		obj.display();
		System.out.println("************************************************");
		
		System.out.println(obj.search("Ram"));
		System.out.println("************************************************");
		obj.checkPelindrome();
	}

}
