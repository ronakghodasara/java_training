package com.lt.client;

import com.lt.service.Nokia1100;
import com.lt.service.NokiaLumia;

public class NokiaClient {

	public static void main(String[] args) {
		
		Nokia1100 Cli = new Nokia1100();
		NokiaLumia lumiaClient = new NokiaLumia();
		
		Cli.doConverse();
		Cli.sendSMS();
		
		lumiaClient.doConverse();
		lumiaClient.sendSMS();
		lumiaClient.playVideo();
		
		lumiaClient.do2gConverse();

	}
}
